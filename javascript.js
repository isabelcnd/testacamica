"use strict";

document.getElementById("mostrarInformacion").onclick = function(event) {
    console.log(event);
    let span = document.createElement('span');
    let parent = document.getElementsByClassName("infoRelevante")[0];
    // avisamos que se esta esperando una respuesta
    span.className = 'loading';
    span.innerText =  `Cargando tu localizacion...`; 
    parent.appendChild(span);

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(calculatePositionAndTime, errorPosition);
    }
};

// eliminamos el loading si ya se obtuvo respuesta
var removeLoading = function(parent) {
    let loading = document.getElementsByClassName('loading')[0];
    parent.removeChild(loading);
} 

var calculatePositionAndTime = function(position) {
    let actualTime = new Date(position.timestamp);
    let span = document.createElement('span');
    let parent = document.getElementsByClassName("infoRelevante")[0];

    removeLoading(parent);

    // agregamos el hijo con la informacion obtenida
    span.className = 'tiempo';
    span.innerText =  `La fecha y hora de ejeucion fue a las : ${actualTime.toLocaleString()}`; 
    let br = document.createElement("br");
    parent.appendChild(br);
    parent.appendChild(span);

    document.getElementsByClassName("ocultarInformacion")[0].removeAttribute("disabled")
}
var errorPosition = function(error) {
    let parent = document.getElementsByClassName("infoRelevante")[0];
    removeLoading(parent);

    alert(error.message);
}

var ocultarInfo = function(event) {
    console.log("ocultar info clickeado");
    console.log(event);

    // var keepChild = document.querySelector(".infoRelevante").firstElementChild;
    document.getElementsByClassName("ocultarInformacion")[0].setAttribute("disabled", true);
    
    let childLength = document.querySelector(".infoRelevante").childElementCount;
    if (childLength > 1) {
        let parent = document.querySelector(".infoRelevante");
        for (let index = childLength-1; index > 0; index--) {
            let son = document.querySelector(".infoRelevante").children[index];
            parent.removeChild(son);
        }
    }    
}

// click desde el js con eventListener
document.getElementsByClassName("ocultarInformacion")[0].addEventListener("click", ocultarInfo);

// bloqueamos el boton de ocultar info
document.getElementsByClassName("ocultarInformacion")[0].setAttribute("disabled", true);

/* Actividad */
// centrar todos los elementos del body
// Agregar estilos a los botones
// Modificar el texto del boton ocultar informacion por "ocultar datos"

// diferencias entre querySelector y getElement (https://www.reddit.com/r/learnjavascript/comments/356k1v/confused_on_queryselector_and_getelementbyid/)